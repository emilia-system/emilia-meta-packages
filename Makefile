SHELL := /bin/bash
VERSION=

define generate_package
	@sed -i "s/_desktop=.*/_desktop=$(1)/" PKGBUILD
	@makepkg -d #-d makes it ignore dependencies, for a meta it's mandatory!
	@sed -i "s/_desktop=.*/_desktop=$(1)-apps/" PKGBUILD
	@makepkg -d

endef

meta-packages: gnome plasma xfce cinnamon mate lxqt budgie
	@sed -i "s/_desktop=.*/_desktop=/" PKGBUILD
	@echo -e "\e[31m --> Done! \e[93m Get your packages! \e[39m"

gnome: src/gnome.list src/gnome-apps.list
	$(call generate_package,gnome)
plasma: src/plasma.list src/plasma-apps.list
	$(call generate_package,plasma)
xfce: src/xfce.list src/xfce-apps.list
	$(call generate_package,xfce)
cinnamon: src/cinnamon.list src/cinnamon-apps.list
	$(call generate_package,cinnamon)
mate: src/mate.list src/mate-apps.list
	$(call generate_package,mate)
lxqt: src/lxqt.list src/lxqt-apps.list
	$(call generate_package,lxqt)
budgie: src/budgie.list src/budgie-apps.list
	$(call generate_package,budgie)
